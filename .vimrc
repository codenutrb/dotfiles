set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=/usr/local/opt/fzf
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'Townk/vim-autoclose'
Plugin 'maksimr/vim-jsbeautify'
Plugin 'tmhedberg/matchit'
Plugin 'mtth/scratch.vim'
Plugin 'Chiel92/vim-autoformat'
Plugin 'gcmt/breeze.vim'
Plugin 'ap/vim-css-color'
Plugin 'mattn/emmet-vim'
Plugin 'tpope/vim-surround'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'
Plugin 'mhinz/vim-signify'
Plugin 'scrooloose/nerdtree'
Plugin 'rking/ag.vim'
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'tpope/vim-rails'
Plugin 'vim-ruby/vim-ruby'
Plugin 'tpope/vim-endwise'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'thoughtbot/vim-rspec'
Plugin 'tpope/vim-jdaddy'
Plugin 'mxw/vim-jsx'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'elzr/vim-json'
Plugin 'othree/yajs.vim'
Plugin 'valloric/youcompleteme'
Plugin 'tpope/vim-fireplace'
Plugin 'isRuslan/vim-es6'
Plugin 'pangloss/vim-javascript'
Plugin 'tmux-plugins/vim-tmux-focus-events'
Plugin 'nelstrom/vim-qargs'
Plugin 'altercation/vim-colors-solarized'
Plugin 'Raimondi/delimitMate'
Plugin 'othree/html5.vim'
Plugin 'Valloric/MatchTagAlways'
Plugin 'alfredodeza/pytest.vim'
Plugin 'xolox/vim-notes'
Plugin 'xolox/vim-misc'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'Vimjas/vim-python-pep8-indent'
Plugin 'python-mode/python-mode'
Plugin 'fatih/vim-go'
Plugin 'vimwiki/vimwiki'
Plugin 'ryanoasis/vim-devicons'
Plugin 'tyrannicaltoucan/vim-quantum'
Plugin 'arcticicestudio/nord-vim'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'itchyny/lightline.vim'
Plugin 'ap/vim-buftabline'
Plugin 'janko-m/vim-test'
Plugin 'benmills/vimux'
Plugin 'vim-scripts/StripWhiteSpaces'
Plugin 'tpope/vim-commentary'
Plugin 'majutsushi/tagbar'
Plugin 'bkad/CamelCaseMotion'
Plugin 'junegunn/gv.vim'
Plugin 'junegunn/fzf.vim'
Plugin 'reinh/vim-makegreen'
Plugin 'tpope/vim-dispatch'
Plugin 'skywind3000/asyncrun.vim'

call vundle#end()

syntax on
set encoding=utf8
set bs=2
set t_Co=256
set autoindent
set ruler
set autowrite
set number
set ai sw=4 sts=4 et
set laststatus=2
set clipboard=unnamed
set enc=utf-8
set antialias
set hidden
set incsearch
set smartcase
set hlsearch
set noswapfile
set autoread
set background=dark
set splitright
set background=dark
colorscheme nord
let g:nord_italic = 1
let g:nord_italic_comments = 1
let g:nord_uniform_status_lines = 1
let g:pymode_python = 'python3'
let g:ycm_server_python_interpreter='/usr/bin/python'

let java_highlight_functions="style"
let java_allow_cpp_keywords=1
let g:syntastic_java_checkers=[]
let g:syntastic_python_checkers=['pyflakes']
let g:syntastic_ruby_checkers=['rubocop']
let g:sytastic_html_checkers=[]
"let g:syntastic_javascript_checkers = ['eslint']
let syntastic_mode_map = { 'passive_filetypes': ['html'] }
let g:pymode_breakpoint=0
"let g:pymode_indent = 0
let $PYTHONUNBUFFERED=1

"let g:pymode_breakpoint = 1
"let g:pymode_breakpoint_bind = '<Leader>p'
"let g:pymode_breakpoint_cmd = 'pdb'
let g:vimwiki_map_prefix = '<Leader>e'
let g:pymode_breakpoint = 0
let test#strategy = "asyncrun"

filetype plugin indent on

runtime! macros/matchit.vim

set mouse=a
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe,*.class,*.pyc,*.bin

let mapleader=","

let g:ctrlp_clear_cache_on_exit = 0
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_match_window = 'bottom,order:ttb,min:1,max:20,results:20'

let g:formatprg_java = "astyle"
let g:formatprg_args_java = "--mode=java --style=ansi -A2s3"
let g:pymode_folding=0

set guioptions-=T
set guioptions-=m
set guioptions-=r
set guioptions-=l
set guioptions-=L

set showtabline=2
let vim_markdown_preview_hotkey='<C-m>'
let g:asyncrun_open = 10

noremap ; :
noremap <C-h> :bprev<CR>
noremap <C-l> :bnext<CR>
noremap <C-c> :silent! bp<bar>sp<bar>bn<bar>bd<CR>
noremap <C-a> ggVG<CR>
noremap <C-p> :Files<CR>

noremap <C-\> :NERDTreeTabsToggle<CR>
noremap <F4> magg=G`a
noremap <F7> :TagbarToggle<CR>

let g:jedi#completions_enabled = 0

set complete=.,w,b,u,t,i

function! JAVASET()
  set tags=~/.tags
  set ai sw=4 sts=4 et
  setlocal makeprg=javac\ -nowarn\ %
  setlocal errorformat=%f:%l:\ %m
  set cindent
  set tw=0
  " setlocal omnifunc=javacomplete#Complete
  " setlocal completefunc=javacomplete#CompleteParamsInfo
  inoremap <buffer> <C-X><C-U> <C-X><C-U><C-P>
  inoremap <buffer> <C-S-Space> <C-X><C-U><C-P>
  map <buffer> <F9> :make<CR>
  map <buffer> <F3> :!echo %\|awk -F. '{print $1}'\|xargs "javac" %:r.java<CR>
  map <buffer> <F5> :!echo %\|awk -F. '{print $1}'\|xargs "java" %:r<CR>
  map <buffer> <F6> :!echo %\|awk -F. '{print $1}'\|cat %:r.in\|java %:r<CR>
endfunction

function! CPPSET()
  setlocal makeprg=g++\ -O2\ -g\ -o\ %.bin\ %
  setlocal errorformat=%f:%l:\ %m
  set cindent
  set tw=0
  set nowrap
  map <buffer> <F9> :make<CR>
  map <buffer> <F5> :!echo %\| awk -F. '{print $1}'\|xargs %:p.bin <CR>
endfunction

function! CSET()
  setlocal makeprg=gcc\ -O2\ -g\ -o\ %.c.bin\ %
  setlocal errorformat=%f:%l:\ %m
  set cindent
  set tw=0
  set nowrap
  map <buffer> <F9> :make<CR>
  map <buffer> <F5> :!echo %\| awk -F. '{print $1}'\|xargs %:p.bin <CR>
endfunction

function! PYSET()
  set tw=0
  setlocal ai sw=4 sts=4 et
  set nowrap

  map <Leader>tf :Pytest file<CR>
  map <Leader>tc :Pytest class<CR>
  map <Leader>tm :Pytest method<CR>
  " map <buffer> <F5> :AsyncRun echo %\|awk -F. '{print $1}'\|xargs python2 % <CR>
  " map <buffer> <F6> :!echo %\|awk -F. '{print $1}'\|cat %:r.in\|python2 % <CR>
  map <buffer> <F5> :w <CR> :AsyncRun -raw python %<CR>
endfunction

function! PHPSET()
  set tw=0
  setlocal ai sw=4 sts=4 ts=4 et
  set nowrap
  map <buffer> <F5> :!echo %\|awk -F. '{print $1}'\|xargs php % <CR>
endfunction

function! JSSET()
  set tw=0
  setlocal ai sw=2 sts=2 ts=2 et
  set nowrap
  map <buffer> <F5> :!echo %\|awk -F. '{print $1}'\|xargs node % <CR>
endfunction

function! SCALASET()
  set nowrap
  set ts=2 sts=2 sw=2
  map <buffer> <F5> :!echo %\|awk -F. '{print $1}'\|xargs scala % <CR>
endfunction

function! RUBYSET()
  setlocal ai sw=2 sts=2 et
  set nowrap
  map <buffer> <F5> :!ruby %<CR>
  map <buffer> <F6> :!echo %\|awk -F. '{print $1}'\|cat %:r.in\|ruby % <CR>
endfunction

function! CLOJURESET()
  setlocal ai sw=2 sts=2 et
  set nowrap
  map <buffer> <F5> :Eval<CR>
endfunction

function! GOSET()
  map <buffer> <F6> :GoRename <CR>
  map <buffer> <F5> :GoRun <CR>
  setlocal noet ts=2 sw=2 sts=2
  map <Leader>o :GoDef<CR>
endfunction

function! JSONSET()
  map <F4> gqaj <CR>
endfunction

function! VIMSET()
  let b:disable_StripWhiteSpaces = 1
endfunction

au BufRead,BufNewFile *.blade.php set filetype=html
au BufRead,BufNewFile *.clj set filetype=clojure

autocmd FileType java call JAVASET()
autocmd FileType cpp call CPPSET()
autocmd FileType c call CPPSET()
autocmd FileType python call PYSET()
autocmd FileType ruby call RUBYSET()
autocmd FileType scala call SCALASET()
autocmd FileType php call PHPSET()
autocmd FileType javascript call JSSET()
autocmd FileType clojure call CLOJURESET()
autocmd FileType go call GOSET()
autocmd FileType json call JSONSET()
autocmd FileType vim call VIMSET()

let NERDTreeIgnore=['\.jnlp', '\.app$', '\.jar$', '\.bak$', '\.pyc$', '\.class$','\.bin$', '\.out$']

set ttimeoutlen=50

let g:cssColorVimDoNotMessMyUpdatetime = 1
let g:ag_working_path_mode="r"

set laststatus=2
let g:bufferline_echo = 0

noremap <Leader>em :Emodel 
noremap <Leader>ec :Econtroller 
noremap <Leader>ev :Eview 
noremap <Leader>f :Ag 
noremap <Leader>g :Ag -G\.  
noremap <leader>nf :NERDTreeFind<cr>
noremap <Leader>pi :so %<CR>:PluginInstall<CR>
noremap <Leader>s :w<CR>
noremap <Leader>q :q! <CR>
noremap <Leader>sq :wq! <CR>
nnoremap <CR> :noh<CR><CR>
nnoremap <Leader>w <C-w>
noremap <Leader>r :%s/
noremap <Leader>vr :Qargs<CR> :argdo %s/
noremap <Leader>vu :argdo update<CR>
noremap <Leader>gs :Gstatus <CR>
noremap <Leader>gc :Gcommit <CR>
noremap <Leader>tn :TestNearest <CR>
noremap <Leader>tf :TestFile <CR>
noremap <Leader>e :edit<CR>

set pastetoggle=<F3>
set noshowmode
set autoread

map <Leader>tm :Pytest method<CR>
let g:rspec_runner = "os_x_iterm2"
let g:rspec_command = "!bundle exec rspec -I . {spec}"

noremap <Leader>q :q! <CR>
noremap <Leader>sq :wq! <CR>
inoremap jj <Esc>
cnoremap jj <C-c>
vno v <esc>

let g:rainbow_active = 0

set nofoldenable
set completeopt-=preview
au FileType python map <silent> <leader>b oimport pdb; pdb.set_trace()<esc>
let delimitMate_expand_cr = 1

let g:strip_whitespace_on_save = 1
let g:better_whitespace_filetypes_blacklist=['vim']

" Triger `autoread` when files changes on disk
" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
" https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
" Notification after file change
" https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
autocmd FileChangedShellPost *
  \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None

" map <silent> w <Plug>CamelCaseMotion_w
" map <silent> b <Plug>CamelCaseMotion_b
" map <silent> e <Plug>CamelCaseMotion_e
" map <silent> ge <Plug>CamelCaseMotion_ge
" sunmap w
" sunmap b
" sunmap e
" sunmap ge
"
let g:go_fmt_command = "goimports"
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1

" Default fzf layout
" - down / up / left / right
let g:fzf_layout = { 'down': '~15%' }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

if !has('gui_running')
  set t_Co=256
endif

set showtabline=2
let g:lightline#bufferline#show_number  = 1
let g:lightline#bufferline#shorten_path = 0
let g:lightline#bufferline#unnamed      = '[No Name]'

let g:lightline = {
      \ 'colorscheme': 'nord',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }
