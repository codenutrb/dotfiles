:if search ('<+FILE_NAME+>')
:  %s/<+FILE_NAME+>/\=expand('%:r')/g
:endif
:if search ('<+CURSOR+>')
:  normal! "_da>
:endif
import java.util.*;
import java.io.*;

public class <+FILE_NAME+> {

    private void solve() throws IOException {
        <+CURSOR+>
    }

    static void debug(Object...o) {
        System.err.println(Arrays.deepToString(o));
    }

    BufferedReader br;
    StringTokenizer st;
    PrintWriter out;

    String next() throws IOException {
        while (st == null || !st.hasMoreTokens()) {
            String s = br.readLine();
            if(s != null) {
                st = new StringTokenizer(s);
            }
        }
        if(st.hasMoreTokens()) {
            return st.nextToken();
        } else {
            return null;
        }
    }

    int nextInt() throws IOException {
        return Integer.parseInt(next());
    }

    double nextDouble() throws IOException {
        return Double.parseDouble(next());
    }

    long nextLong() throws IOException {
        return Long.parseLong(next());
    }

    String nextLine(){
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void run() throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));
        out = new PrintWriter(System.out);
        solve();
        out.close();
    }

    public static void main(String[] args) throws IOException {
        new <+FILE_NAME+>().run();
    }
}
