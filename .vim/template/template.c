:if search ('<+FILE_NAME+>')
:  %s/<+FILE_NAME+>/\=expand('%:r')/g
:endif
:if search ('<+CURSOR+>')
:  normal! "_da>
:endif
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <string.h>

#define ll long long
#define sz(v) (int) v.size()
#define az(x) (sizeof(x) / sizeof(x[0]))
#define oo (int) 13e7
#define fill(v, e) memset(v, e, sizeof v)

int main() {
   freopen("<+FILE_NAME+>.in", "r", stdin);

   <+CURSOR+>   
}
